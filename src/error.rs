use std::fmt::Formatter;

#[derive(Debug)]
pub struct Error(String);

impl Error {
    pub fn new<T: ToString>(s: T) -> Self {
        Self(s.to_string())
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Error {
        Error(format!("{}: {}", ek_to_str(e.kind()), e.to_string()))
    }
}

impl From<Error> for std::io::Error {
    fn from(e: Error) -> Self {
        let estr = e.to_string();
        let eks: Vec<&str> = estr.split(':').collect();

        
        let ek = str_to_ek(eks[0]);

        let txt: &str = if eks.len() == 2 { eks[1] } else { "" };

        std::io::Error::new(ek, txt)
    }
}

fn ek_to_str(ek: std::io::ErrorKind) -> &'static str {
    use std::io::ErrorKind;
    match ek {
        ErrorKind::NotFound => "entity not found",
        ErrorKind::PermissionDenied => "permission denied",
        ErrorKind::ConnectionRefused => "connection refused",
        ErrorKind::ConnectionReset => "connection reset",
        ErrorKind::ConnectionAborted => "connection aborted",
        ErrorKind::NotConnected => "not connected",
        ErrorKind::AddrInUse => "address in use",
        ErrorKind::AddrNotAvailable => "address not available",
        ErrorKind::BrokenPipe => "broken pipe",
        ErrorKind::AlreadyExists => "entity already exists",
        ErrorKind::WouldBlock => "operation would block",
        ErrorKind::InvalidInput => "invalid input parameter",
        ErrorKind::InvalidData => "invalid data",
        ErrorKind::TimedOut => "timed out",
        ErrorKind::WriteZero => "write zero",
        ErrorKind::Interrupted => "operation interrupted",
        ErrorKind::Other => "other os error",
        ErrorKind::UnexpectedEof => "unexpected end of file",
        ErrorKind::Unsupported => "unsupported",
        ErrorKind::OutOfMemory => "out of memory",
        _ => "",
    }
}

fn str_to_ek(s: &str) -> std::io::ErrorKind {
    use std::io::ErrorKind;
    match s {
        "entity not found" => ErrorKind::NotFound,
        "permission denied" => ErrorKind::PermissionDenied,
        "connection refused" => ErrorKind::ConnectionRefused,
        "connection reset" => ErrorKind::ConnectionReset,
        "connection aborted" => ErrorKind::ConnectionAborted,
        "not connected" => ErrorKind::NotConnected,
        "address in use" => ErrorKind::AddrInUse,
        "address not available" => ErrorKind::AddrNotAvailable,
        "broken pipe" => ErrorKind::BrokenPipe,
        "entity already exists" => ErrorKind::AlreadyExists,
        "operation would block" => ErrorKind::WouldBlock,
        "invalid input parameter" => ErrorKind::InvalidInput,
        "invalid data" => ErrorKind::InvalidData,
        "timed out" => ErrorKind::TimedOut,
        "write zero" => ErrorKind::WriteZero,
        "operation interrupted" => ErrorKind::Interrupted,
        "unexpected end of file" => ErrorKind::UnexpectedEof,
        "unsupported" => ErrorKind::Unsupported,
        "out of memory" => ErrorKind::OutOfMemory,
        _ => ErrorKind::Other,
    }
}
