#![feature(with_options)]

mod error;
use error::Error;

use std::fmt::Formatter;
use std::fs::File;
use std::io::Read;

#[derive(Debug, Clone)]
struct Note {
    name: String,
    filename: String,
    date: String,
    // note_id: u32,
}

impl Note {
    pub fn from_str(s: &str) -> Option<Self> {
        let s = s.to_string();

        if s == "title, filename, date" || s.is_empty() {
            return None;
        }

        let v: Vec<&str> = s.split(',').collect();
        let vec: Vec<String> = v.iter().map(|x| x.trim().to_string()).collect();

        if vec.len() == 3 {
            Some(Note {
                name: vec[0].clone(),
                filename: vec[1].clone(),
                date: vec[2].clone(),
            })
        } else {
            None
        }
    }
}

impl std::fmt::Display for Note {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}, {}", self.name, self.filename, self.date)
    }
}

struct Database(Vec<Note>);

impl Database {
    pub fn open(file: &mut File) -> Result<Self, Error> {
        let mut buf: String = String::new();
        file.read_to_string(&mut buf)?;

        let lines: Vec<&str> = buf.split('\n').collect();
        let mut vec: Vec<Note> = Vec::new();

        for line in lines {
            if let Some(n) = Note::from_str(line) {
                vec.append(&mut vec![n; 1]);
            }
        }

        if vec.is_empty() {
            Err(Error::new("DB Vec is empty"))
        } else {
            Ok(Self(vec))
        }
    }
}

impl std::fmt::Display for Database {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut str = String::new();

        self.0
            .iter()
            .for_each(|note| str = format!("{}\n{}", str, note));

        write!(f, "{}", str)
    }
}

fn main() -> Result<(), Error> {
    let mut dbf: File = File::with_options()
        .read(true)
        .write(true)
        .open("notes.csv")?;

    let db: Database = Database::open(&mut dbf)?;

    println!("{}", db);

    Ok(())
}
